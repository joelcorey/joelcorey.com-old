<?php

require 'vendor/autoload.php';
require 'conf.php';
use Mailgun\Mailgun;

$email = $_POST['n_email'];
$message = $_POST['n_message'];

$mgClient = new Mailgun($key);

$result = $mgClient->sendMessage($domain, array(
    'from'    => $email,
    'to'      => $to,
    'subject' => $subject,
    'text'    => $message
));

header('Location: index.html');